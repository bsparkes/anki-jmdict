import json
import os
from aqt import mw, gui_hooks
from aqt.utils import showInfo, getText
from aqt.qt import *
from anki.storage import Collection

from .search import *
from .pitch import *
from .ankiInteraction import *
from .classes.configC import *

from .大辞林Search import *



configFile = "config.json"

mainConfig = Config()
mainConfig.setByJSON(configFile)


def getResults(searchTerm, config):
	# Return list of dicEntries which match searchTerm.
	# Config specifies db to use

	results = []
	entryIDs = getEntryIDS(searchTerm, config.dicPath)
	results = [dicEntry(entryID, config.dicPath) for entryID in entryIDs]

	return results


def lookup(editor):

	term, term_succeeded = getText('Enter a word. (Example: 探検)')
	if not term_succeeded:
		return

	results = getResults(term, mainConfig)
	choice = None

	if len(results) == 0:
		showInfo(("No results found"))
	elif len(results) == 1:
		choice = 0
	elif len(results) > 1:
		choice = displayChoices(results)

	if choice != None:
		entry = results[choice]
		data = [(fld, editor.mw.col.media.escapeImages(val)) for fld, val in editor.note.items()]
		fillAllFields(editor, entry, data, mainConfig)

		if len(entry.kanjiList) > 0:
			mainKanji = entry.kanjiList[0]
		else:
			mainKanji = entry.kanaList[0]

		detail = getJGloss(editor=editor, kanaFData=entry.kanaList[0], kanjFData=mainKanji, config=mainConfig)
		updateField(editor, data, mainConfig.jGlossFN, detail)

		pitchSVG = getSVG(mainConfig.pitchDB, mainKanji, entry.kanaList[0])
		if pitchSVG:
			updateField(editor, data, mainConfig.pitchFN, pitchSVG)


def jGlossLookup(editor):

	data = [(fld, editor.mw.col.media.escapeImages(val)) for fld, val in editor.note.items()]
	kanaFData = getFieldData(editor, data, mainConfig.mainKanaFN)
	kanjFData = getFieldData(editor, data, mainConfig.mainKanjiFN)

	detail = getJGloss(editor=editor, kanaFData=kanaFData, kanjFData=kanjFData, config=mainConfig)
	# Doing this twice seems to work, not sure why.
	# Doing once sometimes works…
	updateField(editor, data, mainConfig.jGlossFN, detail)
	updateField(editor, data, mainConfig.jGlossFN, detail)


def clearJMDictFiled(editor):

	data = [(fld, editor.mw.col.media.escapeImages(val)) for fld, val in editor.note.items()]
	updateField(editor, data, mainConfig.glossFN, "")
	updateField(editor, data, mainConfig.glossFN, "")


def fillAllFields(editor, entry, data, config):

	if len(entry.kanjiList) > 0:
		mainKanji = entry.kanjiList[0]
	else:
		mainKanji = entry.kanaList[0]
	updateField(editor, data, config.mainKanjiFN, mainKanji)

	updateField(editor, data, config.mainKanaFN, entry.kanaList[0])
	updateField(editor, data, config.POSFN, entry.getPOSHTML())
	updateField(editor, data, config.glossFN, entry.getSenseHTML())
	updateField(editor, data, config.altKanjiFN, entry.getAltKanjiHTML())
	updateField(editor, data, config.altKanaFN, entry.getAltKanaHTML())
	updateField(editor, data, config.entryIDFN, entry.getEntryID())


def addEntryIDByMainKanjiKana():

	deckID = select_deck_id('Which deck would you like to add IDs to?')
	if deckID == None:
		return
	note_type_ids = getNoteTypes(deckID)
	# TODO add new note ids
	noteTypeID = note_type_ids[0]
	noteIDs = getNoteIDs(deckID, noteTypeID)
	for noteID in noteIDs:
		updateNote_entryID_KanjiKana(noteID, mainConfig)


def updateEntryies_entryID():

	deckID = select_deck_id('Which deck would you update by entryID?')
	if deckID == None:
		return
	note_type_ids = getNoteTypes(deckID)
	# TODO add new note ids
	noteTypeID = note_type_ids[0]
	noteIDs = getNoteIDs(deckID, noteTypeID)
	for noteID in noteIDs:
		updateNote_all_entryID(noteID, mainConfig)


def addJMdictButton(buttons, editor):
    # environment
	collection_path = mw.col.path
	plugin_dir_name = __name__

	user_dir_path = os.path.split(collection_path)[0]
	anki_dir_path = os.path.split(user_dir_path)[0]
	plugin_dir_path = os.path.join(anki_dir_path, 'addons21', plugin_dir_name)
	icon_path = os.path.join(plugin_dir_path, 'icon.png')

	btn = editor.addButton(icon=icon_path,
												 cmd='foo',
												 func=lookup,
												 tip='search JMdict　(⌘J)',
												 keys=QKeySequence('Ctrl+J')
	 )
	buttons.append(btn)

def addJGlossButton(buttons, editor):
    # environment
	collection_path = mw.col.path
	plugin_dir_name = __name__

	user_dir_path = os.path.split(collection_path)[0]
	anki_dir_path = os.path.split(user_dir_path)[0]
	plugin_dir_path = os.path.join(anki_dir_path, 'addons21', plugin_dir_name)
	icon_path = os.path.join(plugin_dir_path, 'icon.png')

	btn = editor.addButton(icon=icon_path,
												 cmd='foo',
												 func=jGlossLookup,
												 tip='get jGloss　(⌘K)',
												 keys=QKeySequence('Ctrl+K')
	 )
	buttons.append(btn)


def removeGlossButton(buttons, editor):
    # environment
	collection_path = mw.col.path
	plugin_dir_name = __name__

	user_dir_path = os.path.split(collection_path)[0]
	anki_dir_path = os.path.split(user_dir_path)[0]
	plugin_dir_path = os.path.join(anki_dir_path, 'addons21', plugin_dir_name)
	icon_path = os.path.join(plugin_dir_path, 'icon.png')

	btn = editor.addButton(icon=icon_path,
												 cmd='remove JMDict Gloss',
												 func=clearJMDictFiled,
												 tip='remove JMDict Gloss　(⌘R)',
												 keys=QKeySequence('Ctrl+R')
	 )
	buttons.append(btn)

# def JGlossShortCut(shortcuts, editor):
# 	# Shortcut does not give arguments
# 	shortcuts.append((QKeySequence('Ctrl+K'), jGlossLookup))


# add editor button
gui_hooks.editor_did_init_buttons.append(addJMdictButton)
gui_hooks.editor_did_init_buttons.append(addJGlossButton)
gui_hooks.editor_did_init_buttons.append(removeGlossButton)

sJDI_menu = QMenu('Simple JDI', mw)

sJDI_menu_test = sJDI_menu.addAction('Add EntryIDs to notes by main kanji and kana')
sJDI_menu_test.triggered.connect(addEntryIDByMainKanjiKana)

sJDI_menu_test = sJDI_menu.addAction('Redo notes by entryID')
sJDI_menu_test.triggered.connect(updateEntryies_entryID)

mw.form.menuTools.addMenu(sJDI_menu)